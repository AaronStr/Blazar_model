################################################################################
# Macro that evaluate cooling of electrons due to synchrotron losses
# eqns. (11) for the PDE, (34) for the losses, (64) for the sources 
# in Mastichiadis, Kirk (1995)
# In this version v2 I use the odeint for ODE integration from scipy
# cox 29 Jun 2016
################################################################################

from __future__ import division
import numpy as np
from math import log10, pi, sqrt
from scipy.integrate import odeint
import matplotlib.pyplot as plt


################################################################################
# defining physical constants
################################################################################
b = 1e-6 		# magnetic field in order of the critical field b = B/B_c
l_b = 0.075 	# magnetic compactness
Q_norm = 1e-1 	# normalization in Y for the power-law of electrons
s = 2 			# power law index of injected e-


################################################################################
# definition of functions
################################################################################

def Qe(Y):
	'''defining source term for e- (power-law)'''
	return Q_norm*Y**(-s)

def e_analytic_cooling(Y, Y_max, t):
	'''analytic solution: 
	   the break in the gamma spectrum depends on the time and on the maximum Lorentz factor, then we have two solutions pre and post break'''
		
	Y_break = Y_max/(1 + 4*Y_max*l_b*t/3)

	def prebreak_solution(y):
		return 3*Qe(y)/(4*l_b*(s-1)) * (1/y - 1/y*(1 - (4*l_b*y*t)/3)**(s-1))

	def postbreak_solution(y):
		return (3*Q_norm)/(4*l_b*(s-1)*y**2)*(y**(-s+1) - Y_max**(-s+1))
	
	return np.array([ prebreak_solution(y) if y < Y_break else postbreak_solution(y) for y in Y])


################################################################################
# main
################################################################################

# intial definitions for the plots
plot_count = 0
t_max_list = [1., 1e-1, 1e-2, 1e-3, 1e-4] # list of maximum times in which we want to step
color_list = ['firebrick', 'peru', 'goldenrod', 'seagreen', 'darkcyan']
label_list = [r'$t = 1\,t_{\rm cross}$', r'$t = 10^{-1}\,t_{\rm cross}$', r'$t = 10^{-2}\,t_{\rm cross}$', r'$t = 10^{-3}\,t_{\rm cross}$', r'$t = 10^{-4}\,t_{\rm cross}$']

fig = plt.figure()
font = {'family': 'serif',  'color':'black', 'weight': 'normal', 'size': 16.} # font definitions
ax0 = fig.add_subplot(1, 1, 1)

# plot a e- cooling spectrum for each maximum time
for t_max in t_max_list:

	N_t = 100
	# defining the timestep delta_t, using t_min, (t_max already defined by the loop variable), and the initial number of timesteps (N_t)
	t_min = 1e-6; times = np.linspace(t_min, t_max, N_t); 
	# defining dimenison of the grid in Lorentz factor of e- (Y) 
	N_Y = 500
	# minimum, maximum Lorentz factor for e-, array of gamma points
	Y_min = 3.; Y_max = 3.e5; Y = np.logspace(log10(Y_min), log10(Y_max), N_Y)

	# the PDE is discretized as an ODE: dn/dt = Q + L is now an array with the same shape of Y that will be evolved in time by odeint
	def odefunc(ne, t):
		dne_dt = np.zeros(Y.shape)
		dne_dt[0] =  (3*Q_norm)/(4*l_b) * (4/3*l_b)**(s-1) * Y[0]**(-2) * t**(s-2) # boundary condition: derivative of the analytical solution evaluated at Y[0]
	
		# loop on the energies
		for i in range(1, N_Y-1):
			dne_dt[i] = Qe(Y[i]) + 4/3*l_b*(Y[i+1]**2*ne[i+1] - Y[i]**2*ne[i])/(Y[i+1] - Y[i])

		return dne_dt
	
	# the initial condition is the analytical solution at t_0 (t_min)	
	ne_init = e_analytic_cooling(Y, Y_max, t_min)
	
	# use the odeint to evolve the PDE discretized in odefunc definition
	ne_evolved = odeint(odefunc, ne_init, times)
	 
	# plot section
	ax0.plot(np.log10(Y), np.log10(Y**2*ne_evolved[-1]), color = color_list[plot_count], linestyle = '-', linewidth = 2.2, label = 'Numerical ' + label_list[plot_count])
	ax0.plot(np.log10(Y), np.log10(Y**2*e_analytic_cooling(Y, Y_max, t_max)), color = 'black', linestyle = '--', linewidth = 1.8)
	
	plot_count += 1 # update the plot counter 

# rest of the plotting section
ax0.plot(0., 0., color = 'black', linestyle = '--', linewidth = 1.8, label = 'Analytical check') # dummy point printed just to show the label of the analytical solutions in the grid
ax0.set_title('Electron synchrotron cooling', y=1.02)
ax0.legend(loc = 1., numpoints = 1., prop = {'size':12.})
ax0.xaxis.grid(True)
ax0.yaxis.grid(True)
ax0.set_ylabel(r'${\rm log}(\gamma^2 n_e)$', fontdict = font)
ax0.set_xlabel(r'${\rm log}(\gamma)$', fontdict = font)
ax0.set_ylim(-7,0)
plt.show()
fig.savefig("synchro_cooling_e_scipy-odeint_v2.eps")


